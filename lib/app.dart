import 'package:flutter/material.dart';
import 'package:infinitelist_bloc/posts/view/post_page.dart';


class App extends MaterialApp {
  App() : super(home: PostsPage());
}
